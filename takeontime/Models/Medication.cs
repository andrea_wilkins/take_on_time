﻿using System;  
using SQLite.Net.Attributes;  
  
namespace takeontime.Models
{
    [Table("Medication")]
    public class Medication
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public string Unit { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public TimeSpan Time { get; set; }
        public string TimeString { get; set; }
        public string Frequency { get; set; }
        public string Remaining { get; set; }
        public bool Remind { get; set; }
        public string FullUnit => $"{Dosage} {Unit}";
    }
}
