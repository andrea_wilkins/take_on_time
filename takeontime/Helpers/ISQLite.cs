﻿using System;
using SQLite.Net;
namespace takeontime.Helpers
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
