﻿using SQLite.Net;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using takeontime.Models;
using System;

namespace takeontime.Helpers
{
    public class DatabaseHelper
    {

        static SQLiteConnection sqliteconnection;
        public const string DbFileName = "Medications.db";

        public DatabaseHelper()
        {
            sqliteconnection = DependencyService.Get<ISQLite>().GetConnection();
            sqliteconnection.CreateTable<Medication>();
        }

        // Get All Medication data      
        public List<Medication> GetAllMedicationData()
        {
            return (from data in sqliteconnection.Table<Medication>()
                    select data).ToList();
        }

        //Get Specific Medication data  
        public Medication GetMedicationData(int id)
        {
            return sqliteconnection.Table<Medication>().FirstOrDefault(t => t.Id == id);
        }

        // Delete all Medication Data  
        public void DeleteAllMedications()
        {
            sqliteconnection.DeleteAll<Medication>();
        }

        //Delete Specific Medication  
        public void DeleteMedication(int id)
        {
            sqliteconnection.Delete<Medication>(id);
        }

        // Insert new Medication to DB   
        public void InsertMedication(Medication medication)
        {
            sqliteconnection.Insert(medication);
        }

        // Update Medication Data  
        public void UpdateMedication(Medication medication)
        {
            sqliteconnection.Update(medication);
        }
    }
}
