﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.LocalNotification;

namespace takeontime
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //NotificationCenter.Current.NotificationTapped += OnLocalNotificationTapped;
            MainPage = new MyTabbedPage() {
                SelectedTabColor = Color.FromHex("#91C7A3"),
                //BarBackgroundColor = Color.FromHex("#3CA3A2"),                 //BarTextColor = Color.White
            };
        }

        //private void OnLocalNotificationTapped(LocalNotificationTappedEvent e)
        //{
        //    // your code goes here
        //}

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
