﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using takeontime.Services;
using takeontime.ViewModels;
using Xamarin.Forms;

namespace takeontime
{
    public partial class MedsPage : ContentPage
    {
        public MedsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, true);
        }

        protected override void OnAppearing()
        {
            this.BindingContext = new MedicationListViewModel(Navigation);
        }
    }
}
