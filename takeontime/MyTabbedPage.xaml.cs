﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace takeontime
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTabbedPage : TabbedPage
    {
        public MyTabbedPage()
        {
            InitializeComponent();

            var homePage = new NavigationPage(new HomePage());
            homePage.Title = "Home";
            homePage.IconImageSource = "home.png";
            homePage.BarBackgroundColor = Color.FromHex("#3CA3A2");
            homePage.BarTextColor = Color.White;

            var medsPage = new NavigationPage(new MedsPage());
            medsPage.IconImageSource = "pill.png";
            medsPage.Title = "Medication";
            medsPage.BarBackgroundColor = Color.FromHex("#3CA3A2");
            medsPage.BarTextColor = Color.White;

            var profilePage = new NavigationPage(new ProfilePage());
            profilePage.IconImageSource = "profile.png";
            profilePage.Title = "Profile";
            profilePage.BarBackgroundColor = Color.FromHex("#3CA3A2");
            profilePage.BarTextColor = Color.White;

            Children.Add(homePage);
            Children.Add(medsPage);
            Children.Add(profilePage);

            CurrentPage = Children[0];
        }
    }
}
