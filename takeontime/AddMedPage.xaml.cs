﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using takeontime.Models;
using takeontime.ViewModels;
using Xamarin.Forms;

namespace takeontime
{
    public partial class AddMedPage : ContentPage
    {
        public AddMedPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            BindingContext = new AddMedicationViewModel(Navigation);
        }
    }
}
