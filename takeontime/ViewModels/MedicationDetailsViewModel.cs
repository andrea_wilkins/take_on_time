﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using takeontime.Helpers;
using takeontime.Models;
using takeontime.Services;
using Xamarin.Forms;

namespace takeontime.ViewModels
{
    public class MedicationDetailsViewModel : BaseMedicationViewModel
    {
        public ICommand UpdateMedicationCommand { get; private set; }
        public ICommand DeleteMedicationCommand { get; private set; }

        public MedicationDetailsViewModel(INavigation navigation, int selectedMedicationID)
        {
            _navigation = navigation;
            _currentMedication = new Medication();
            _currentMedication.Id = selectedMedicationID;
            _medicationRepository = new MedicationRepository();

            UpdateMedicationCommand = new Command(async () => await UpdateMedication());
            DeleteMedicationCommand = new Command(async () => await DeleteMedication());

            FetchMedicationDetails();
        }

        void FetchMedicationDetails()
        {
            _currentMedication = _medicationRepository.GetMedicationData(_currentMedication.Id);
        }

        async Task UpdateMedication()
        {
           
                bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Medication Details", "Are you sure you want to update these medication details", "Yes", "Cancel");
                if (isUserAccept)
                {
                _medicationRepository.UpdateMedication(_currentMedication);
                    await _navigation.PopAsync();
                }
          
        }

        async Task DeleteMedication()
        {
            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Wait!", "Are you sure you want to delete this medication?", "Yes", "Cancel");
            if (isUserAccept)
            {
                _medicationRepository.DeleteMedication(_currentMedication.Id);
                await _navigation.PopAsync();
            }
        }
    }
}
