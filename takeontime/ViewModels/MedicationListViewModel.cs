﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using takeontime.Models;
using takeontime.Services;
using takeontime.ViewModels;
using Xamarin.Forms;

namespace takeontime.ViewModels
{
    public class MedicationListViewModel : BaseMedicationViewModel
    {
        public ICommand AddCommand { get; private set; }
        public ICommand DeleteAllMedicationsCommand { get; private set; }

        public MedicationListViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _medicationRepository = new MedicationRepository();

            AddCommand = new Command(async () => await ShowAddMedication());
            DeleteAllMedicationsCommand = new Command(async () => await DeleteAllMedications());

            FetchMedications();
        }

        void FetchMedications()
        {
            MedicationList = _medicationRepository.GetAllMedicationData();
        }

        async Task ShowAddMedication()
        {
            await _navigation.PushAsync(new AddMedPage());
        }

        async Task DeleteAllMedications()
        {
            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Wait!", "Are you sure you want to delete all medications?", "Yes", "Cancel");
            if (isUserAccept)
            {
                _medicationRepository.DeleteAllMedications();
                await _navigation.PushAsync(new MedsPage());
            }
        }

        async void ShowMedicationDetails(int selectedMedicationID)
        {
            await _navigation.PushAsync(new MedicationDetailsPage(selectedMedicationID));
        }

        Medication _selectedMedicationItem;
        public Medication SelectedMedicationItem
        {
            get => _selectedMedicationItem;
            set
            {
                if (value != null)
                {
                    _selectedMedicationItem = value;
                    NotifyPropertyChanged("_selectedMedicationItem");
                    ShowMedicationDetails(value.Id);
                }
            }
        }
    }
}
