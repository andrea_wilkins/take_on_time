﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using takeontime.Helpers;
using takeontime.Models;
using takeontime.Services;
using takeontime.ViewModels;
using Xamarin.Forms;
using Plugin.LocalNotification;

namespace takeontime.ViewModels 
{
    public class AddMedicationViewModel : BaseMedicationViewModel
    {
        public ICommand AddMedicationCommand { get; private set; }

        public AddMedicationViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _currentMedication = new Medication();
            _medicationRepository = new MedicationRepository();

            AddMedicationCommand = new Command(async () => await AddMedication());
        }

        async Task AddMedication()
        {

            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Add Medication", "Are you sure you want to add this to your list of medications?", "Yes", "Cancel");
            if (isUserAccept)
            {
                _medicationRepository.InsertMedication(_currentMedication);
                await _navigation.PushAsync(new MedsPage());

                var notification = new NotificationRequest
                {
                    NotificationId = 100,
                    Title = "Congratulations!",
                    Description = "You just added a new Medication to your list!",
                    //ReturningData = "Dummy data" // Returning data when tapped on notification.
                    NotifyTime = DateTime.Now.AddSeconds(5) // Used for Scheduling local notification, if not specified notification will show immediately.
                };
                NotificationCenter.Current.Show(notification);
            }
     
        }
    }
}
