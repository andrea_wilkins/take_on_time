﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using takeontime.Helpers;
using takeontime.Models;
using takeontime.Services;
using Xamarin.Forms;

namespace takeontime.ViewModels
{
    public class BaseMedicationViewModel : INotifyPropertyChanged
    {
        public Medication _currentMedication;

        public INavigation _navigation;
        public IMedicationRepository _medicationRepository;

        public string Name
        {
            get => _currentMedication.Name;
            set
            {
                _currentMedication.Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string Dosage
        {
            get => _currentMedication.Dosage;
            set
            {
                _currentMedication.Dosage = value;
                NotifyPropertyChanged("Dosage");
            }
        }

        public string Unit
        {
            get => _currentMedication.Unit;
            set
            {
                _currentMedication.Unit = value;
                NotifyPropertyChanged("Unit");
            }
        }

        public DateTime Start
        {
            get => _currentMedication.Start;
            set
            {
                _currentMedication.Start = value;
                NotifyPropertyChanged("Start");
            }
        }

        public DateTime End
        {
            get => _currentMedication.End;
            set
            {
                _currentMedication.End = value;
                NotifyPropertyChanged("End");
            }
        }

        public TimeSpan Time
        {
            get => _currentMedication.Time;
            set
            {
                _currentMedication.Time = value;
                NotifyPropertyChanged("Time");
            }
        }


        public string TimeString
        {
            get => _currentMedication.TimeString;
            set
            {
                _currentMedication.TimeString = Time.ToString();
                NotifyPropertyChanged("TimeString");
            }
        }

        public string Remaining
        {
            get => _currentMedication.Remaining;
            set
            {
                _currentMedication.Remaining = value;
                NotifyPropertyChanged("Remaining");
            }
        }

        public bool Remind
        {
            get => _currentMedication.Remind;
            set
            {
                _currentMedication.Remind = value;
                NotifyPropertyChanged("Remind");
            }
        }

        List<Medication> _medicationList;
        public List<Medication> MedicationList
        {
            get => _medicationList;
            set
            {
                _medicationList = value;
                NotifyPropertyChanged("MedicationList");
            }
        }

        #region INotifyPropertyChanged      
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

