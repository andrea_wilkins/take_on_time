﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using takeontime.Models;
using takeontime.Services;
using takeontime.ViewModels;
using Xamarin.Forms;

namespace takeontime.ViewModels
{
    public class HomeViewModel : BaseMedicationViewModel
    {
        public HomeViewModel(INavigation navigation)
        {
            _medicationRepository = new MedicationRepository();
            FetchMedications();

        }

        void FetchMedications()
        {
            MedicationList = _medicationRepository.GetAllMedicationData();
        }

        //void FetchMedications()
        //{
        //    MedicationList = _medicationRepository.GetAllMedicationData();

        //    //for (int i = 0; i < MedicationList.Count; i++) {
        //    //}
        //}
    }
}
