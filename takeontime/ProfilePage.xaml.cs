﻿using System;
using System.Collections.Generic;
using Xamarin.Essentials;

using Xamarin.Forms;

namespace takeontime
{
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();

            BindingContext = this;

            RestoreProfileData();
         
        }

        void Handle_Call_Clicked(object sender, System.EventArgs e)
        {
            if (Application.Current.Properties.ContainsKey("DoctorNumber"))
            {
                string docNumber = Application.Current.Properties["DoctorNumber"].ToString();
                PlacePhoneCall(docNumber);
            } else
            {
                DisplayAlert("Unsuccessful", "Please add a Doctor's number to your profile.", "OK");
            }
        }

        void Handle_Update_Clicked(object sender, System.EventArgs e)
        {
            UpdateProfile();
            DisplayAlert("Success", "Your details have been updated!", "OK");
        }

        void UpdateProfile()
        {
            string name = NameField.Text;
            string surname = SurnameField.Text;
            string condition = ConditionField.Text;
            string docName = DocNameField.Text;
            string docNumber = DocNumberField.Text;

            Application.Current.Properties["Name"] = name;
            Application.Current.Properties["Surname"] = surname;
            Application.Current.Properties["Condition"] = condition;
            Application.Current.Properties["DoctorName"] = docName;
            Application.Current.Properties["DoctorNumber"] = docNumber;

            Application.Current.SavePropertiesAsync();
        }

        void RestoreProfileData()
        {
            if (Application.Current.Properties.ContainsKey("Name"))
            {
                NameField.Text = Application.Current.Properties["Name"].ToString();
            }

            if (Application.Current.Properties.ContainsKey("Surname"))
            {
                SurnameField.Text = Application.Current.Properties["Surname"].ToString();
            }

            if (Application.Current.Properties.ContainsKey("Condition"))
            {
                ConditionField.Text = Application.Current.Properties["Condition"].ToString();
            }

            if (Application.Current.Properties.ContainsKey("DoctorName"))
            {
                DocNameField.Text = Application.Current.Properties["DoctorName"].ToString();
            }

            if (Application.Current.Properties.ContainsKey("DoctorNumber"))
            {
                DocNumberField.Text = Application.Current.Properties["DoctorNumber"].ToString();
            }
        }

        void ClearAppStorage()
        { 
            Application.Current.Properties.Clear();
            Application.Current.SavePropertiesAsync();
        }

        void PlacePhoneCall(string number)
        {
            try
            {
                PhoneDialer.Open(number);
            }
            catch (ArgumentNullException)
            {
                DisplayAlert("Unsuccessful", "This number does not exist.", "OK");
            }
            catch (FeatureNotSupportedException)
            {
                DisplayAlert("Unsuccessful", "Your device does not support this feature.", "OK");
            }
        }
    }
}
