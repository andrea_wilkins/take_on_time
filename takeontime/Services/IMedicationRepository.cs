﻿using System;
using System.Collections.Generic;
using takeontime.Models;

namespace takeontime.Services
{
    public interface IMedicationRepository
    {
        List<Medication> GetAllMedicationData();

        //Get Specific Medication data  
        Medication GetMedicationData(int medicationID);

        // Delete all Medication Data  
        void DeleteAllMedications();

        // Delete Specific Medication  
        void DeleteMedication(int medicationID);

        // Insert new Medication to DB   
        void InsertMedication(Medication medication);

        // Update Medication Data  
        void UpdateMedication(Medication medication);
    }
}
