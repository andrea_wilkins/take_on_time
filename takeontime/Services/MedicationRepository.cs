﻿using System;
using System.Collections.Generic;
using takeontime.Helpers;
using takeontime.Models;

namespace takeontime.Services
{
    public class MedicationRepository : IMedicationRepository
    {
        DatabaseHelper _databaseHelper;
        public MedicationRepository()
        {
            _databaseHelper = new DatabaseHelper();
        }

        public void DeleteAllMedications()
        {
            _databaseHelper.DeleteAllMedications();
        }

        public void DeleteMedication(int medicationID)
        {
            _databaseHelper.DeleteMedication(medicationID);
        }

        public List<Medication> GetAllMedicationData()
        {
            return _databaseHelper.GetAllMedicationData();
        }

        public Medication GetMedicationData(int medicationID)
        {
            return _databaseHelper.GetMedicationData(medicationID);
        }

        public void InsertMedication(Medication medication)
        {
            _databaseHelper.InsertMedication(medication);
        }

        public void UpdateMedication(Medication medication)
        {
            _databaseHelper.UpdateMedication(medication);
        }
    }
}
