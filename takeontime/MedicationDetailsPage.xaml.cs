﻿using System;
using System.Collections.Generic;
using takeontime.ViewModels;
using Xamarin.Forms;

namespace takeontime
{
    public partial class MedicationDetailsPage : ContentPage
    {
        public MedicationDetailsPage(int medicationID)
        {
            InitializeComponent();
            this.BindingContext = new MedicationDetailsViewModel(Navigation, medicationID);
        }

    }
}
