# take_on_time


How to run the application:

1. Clone the repository onto your local device. 
2. Open the project in Visual Studio.
3. Make sure that the emulator is running takeontime.ios and that it is in debug mode.
4. Change the device to iPhone 8 iOS 12.2
5. Click on play to run the app.

Features and functionality:

- A home page that displays the current date and a list of all medications that need to be taken on that specific day. It includes the time, medication and dosage.
- A medication page that displays a list of all medications that the patient is currently on.
- The medication page contains two buttons; an button to add a new medication and a button to delete all medications in the list.
- When you click on one of the medications, you are directed to a details page.
- The details page contains a form that allows you to update the details of that specific medication, as well as an option to delete that medication.
- When the add new button is clicked, you are direction to an add new page.
- On the add new page, you are able to add a new medication to your list.
- There is also a profile page that stores patient details in local storage. 
- You are also able to update your details on your profile page by simply editing the fields and clicking on update.
- There is also a call doctor button on the profile page. 
- If you click on call doctor, it will open up the number in your phone dailer. 
- If you have not yet entered a number, it will ask you to enter one before calling.
- If your phone does not support call functionality (eg. an emulator), it will notify you of that when you try to click call.
- The app also supports notification functionality. The app will notify you that you have created a new medication 5 seconds after you create it. 