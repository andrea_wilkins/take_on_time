﻿using System;
using System.IO;
using Xamarin.Forms;
using SQLite.Net;
using takeontime.Helpers;
using takeontime.iOS.Implementations;

[assembly: Dependency(typeof(IOSSQLite))]
namespace takeontime.iOS.Implementations
{
    public class IOSSQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder  
            string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder  
            var path = Path.Combine(libraryPath, DatabaseHelper.DbFileName);
            // Create the connection  
            var plat = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();
            var conn = new SQLiteConnection(plat, path);
            // Return the database connection  
            return conn;
        }
    }
}
